# Socks proxy setup guide

- Download the latest alpine image from https://alpinelinux.org/downloads/ and upload to virtualisation platform
- Create new VM with 1 core and 256Mb of memory
- Boot from CD and login as root
- Run `setup-alpine` and follow the prompts and reboot
  - create a "sys" install
  - Add a user named proxy
- Install wireguard and required tooling
  - `apk update && apk add wireguard-tools curl`
- As root add wireguard module and reboot
  - `echo wireguard >> /etc/modules && reboot`
- Configure wireguard
  - Create `/etc/wireguard/wg0.conf` with the following template
  - ```
    [Interface]
    Address = <Address provided by VPN service>
    PrivateKey = <Key provided by VPn service>
    
    [Peer]
    Endpoint = <Endpoint provided by VPN Ssrvice>
    PublicKey = <Public key provided by VPN service>
    AllowedIPs = 0.0.0.0/0, ::0/0
    ```
- Test VPN service
  - ```
    wg-quick up wg0
    wg show
    curl ipinfo.io
    ```
- Configure wireguard on startup
  - ```
    cat << EOF > /etc/init.d/wg-quick
    #!/sbin/openrc-run

    description="WireGuard Quick"
    
    depend() {
        need localmount
        need net
    }
    
    start() {
        wg-quick up wg0
    }
    
    stop() {
        wg-quick down wg0
    }
    EOF
    chmod +x /etc/init.d/wg-quick && rc-update add wg-quick default
    ```
- Create ssh keys for proxy user
  ```
  su - proxy 
  ssh-keygen -t rsa -N '' -f /home/proxy/.ssh/id_rsa
  cat ~/.ssh/id_rsa.pub > ~/.ssh/authorized_keys
  ssh-keyscan localhost >> ~/.ssh/known_hosts
  ```
- Confirm ssh works
  - ```
    ssh localhost
    ```
- Enable socks proxy on boot
  - ```
    cat << EOF > /etc/init.d/proxy
    #!/sbin/openrc-run

    description="Socks Proxy"
    command="/usr/bin/ssh"
    command_args="-N -D 0.0.0.0:1080 localhost"
    command_user="proxy"
    pidfile="/run/$RC_SVCNAME/$RC_SVCNAME.pid"
    command_background="yes"
    
    depend() {
        need sshd
    }
    
    start_pre() {
            checkpath --directory --owner $command_user:$command_user --mode 0775 \
                    /run/$RC_SVCNAME
    }
    EOF
    chmod +x /etc/init.d/proxy && rc-update add proxy default
- Reboot and test from your local machine
  - ```
    curl -x socks5h://<proxy ip>:1080 ipinfo.io
    ```
